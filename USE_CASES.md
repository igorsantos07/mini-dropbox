Mini-Dropbox use cases
======================

File transfers
--------------

There's nine possible states of the files. They're listed below and their numbers are used as reference in code commenfs and  Test Cases. the wording uses the client's perspective:

1. **same on the three places**: ignored, **nothing** to do.
2. same on client and server, **different on log**: file created equally at both ends, **nothing** to do.
3. same on client and log, **different on server**: file was synced once but changed in the server; should be **downloaded**.
4. same on server and log, **different on client: file was synced once but changed in the client; should be **uploaded**.
5. same on client and log, **non-existent on server**: file was synced once but deleted on the server; should be **deleted locally**.
6. same on server and log, **non-existent on client**: file was synced once but deleted on the server; should be **deleted remotely**.
7. non-existent on client and log, **present only on server**: file never existed, but sent by someone else to the server; should be **downloaded**.
8. non-existent on server and log, **present only on client**: file never synced, but created locally; should be **uploaded**.
9. **different on the three places**: **not covered** by the assignment. (probably would need to use timestamp verification).


Unusual states
--------------

Everything that's strange or unexpected by the server should cause an specific error code. Common error codes used by the server are described in the server documentation and their causes will not be covered by this file. On the other hand, problems received by the client usually lead it to close itself.
