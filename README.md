Mini-Dropbox
============

Dalhousie CSCI-3171 Network Computing, W2012/2013, Assignment #3
----------------------------------------------------------------

This is a small implementation of client and server of a custom Dropbox-like protocol. It should sync a folder of files (not recursively) between different clients of the same user.

### Implementation details and requirements

* PHP 5.4 *(array dereferencing and small notation, and closure awesomeness!)*
* Depends on the [Pharse] library for command-line options interpretation
* Port `19905` is the default value. It can be changed using one of the options listed below
* For detailed use cases, see `USE_CASES.md`
* Code is being tracked at [BitBucket][vcs] and [issues are there too][issues]. The most updated code can be downloaded as a [ZIP], [GZ] or [BZ2] ball.

### Command-line options
#### Common options
* `--debug`, `-debug`: Puts the system in debug mode, outputing a lot of stuff
* `--help`, `-help`: shows the complete list of options
* `--port`, `-p`: port on which the program will interact
* `--dir`, `-d`: *[required]* the directory that the program will use for synchronization

#### Server implementation
*No specific options*

#### Client implementation
* `--host`, `-h`: host with which the program will interact
* `--user`, `-u`: *[required]* the username used to synchronize
* `--password`, `-q`: *[required]* the password used to synchronize

### Sample usage
`$ mkdir files/client/files`  
`$ ./server.php -d files/server`  
`$ ./client.php -d files/client/files -u charlie -q echo`

### Running tests
The `lib/phpunit.phar` file was included in the project to make it easier to run standalone tests.  
To run them, first run the server separately, and then: `$ ./lib/phpunit.phar tests/`

### Documentation
It was decided to use the new [ApiGen] generator instead of the good'n'old PHPDocumentor because the latter has no support for Traits yet. Since they are widely used in the code, it would make the documentation incomplete.  
The documentation is stored in `docs/`. It can be updated using `$ apigen -p`.


[Pharse]: https://github.com/chrisallenlane/pharse
[vcs]:	  https://bitbucket.org/santos_dal/3171-network-sockets/
[issues]: https://bitbucket.org/santos_dal/3171-network-sockets/issues
[ZIP]:    https://bitbucket.org/santos_dal/3171-network-sockets/get/master.zip
[GZ]:	  https://bitbucket.org/santos_dal/3171-network-sockets/get/master.tar.gz
[BZ2]:	  https://bitbucket.org/santos_dal/3171-network-sockets/get/master.tar.bz2
[ApiGen]: http://apigen.org