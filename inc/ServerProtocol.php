<?
require 'DropProtocol.php';
require 'SummaryFunctions.php';

/**
 * Implements the Protocol part used by the {@link Server}, such as handling the requests and etc.
 * @package Protocol
 */
abstract class ServerProtocol extends DropProtocol {

	/**
	 * Starts the Protocol client.
	 * @param array $summaryOptions The Summary Functions that are available for usage. List of DropProtocol::SUM_*.
	 */
	public function __construct(array $summaryOptions = []) {
		parent::__construct($summaryOptions);
	}

	/**
	 * Should return a ClientMachine object given the $socket.
	 * @return ClientMachine
	 */
	abstract protected function getClientFromSocket($socket);
	/**
	 * Should return the path of a $file for a client identified by the $socket.
	 * @return string
	 */
	abstract protected function getPathOfClientFile($socket, $file);

	/**
	 * Sends a response to a socket.
	 * @param resource $socket The socket to which this response will be sent
	 * @param string $operation One of the constants self::OP_*
	 * @param int $status One of the constants self::ST_*
	 * @param array $headers [optional] Headers to be sent with the response; keys are the name and values are the header values
	 * @param mixed $data Data to be sent in the body
	 */
	protected function response($socket, $operation, $status, array $headers = [], $data = null) {
		$response_line = implode(' ', [self::VERSION, $operation, $status, self::getStatus($status)]);
		$this->sendMsg($socket, $response_line, $headers, $data, $operation == self::OP_GET_FILE);
	}

	/**
	 * Used to handle unidentified operations.
	 * @param string $name
	 * @param mixed $arguments
	 */
	public function __call($name, $arguments) {
		if (substr($name, 0, 2) == 'op')
			$this->response($arguments[0], strtoupper(substr($name, 2)), self::ST_NOT_IMPLEMENTED);
		else
			/** @todo this should be done better, with callstack and stuff */
			throw new Exception("Unknown method $name for ServerProtocol");
	}

	/**
	 * Should appropriately verify and authorize the user to do the other operations.
	 */
	abstract protected function login($socket, $user, $password);

	/**
	 * Responds to a `LOGIN` request, properly authorizing the user if the credentials are right.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opLogin($socket, $path, array $headers, $body = '') {
		list($user, $password) = explode(':', $headers['Authorization']);
		$this->login($socket, $user, $password);
	}

	/**
	 * Responds the client with a choosen summary function. If none can be used, issues a {@link ST_NO_MATCH_FUNC} code.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opExg($socket, $path, array $headers, $body) {
		$server_options = ($this->preferredSummary)? [$this->preferredSummary] : $this->summaryOptions;
		$client_options = explode(LF, $body);

		array_walk($client_options, 'rtrim');
		$available_options = array_intersect($client_options, $server_options);

		if (sizeof($available_options) == 0)
			$this->response($socket, self::OP_EXCHANGE_SELECTION, self::ST_NO_MATCH_FUNC);
		else {
			$this->getClientFromSocket($socket)->setSummary($available_options[0]);
			$this->response($socket, self::OP_EXCHANGE_SELECTION, self::ST_OK, [], $available_options[0]);
		}
	}

	/**
	 * Responds the client with a choosen summary function. If none can be used, issues a {@link ST_NO_MATCH_FUNC} code.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opDir($socket, $path, array $headers, $body) {
		$client = $this->getClientFromSocket($socket);

		$user_dir = $client->getUserDir($this->dir);
		if (!is_dir($user_dir)) mkdir($user_dir);
		$files = CliAndFiles::getLocalFileState($user_dir, $client->summaryFunction);

		$response_body = '';
		foreach ($files as $file => $summary)
			$response_body .= "$file:$summary".LF;

		$response_status = $response_body? self::ST_OK : self::ST_NO_CONTENT;

		$this->response($socket, self::OP_DIRECTORY_LISTING, $response_status, [], $response_body);
	}

	/**
	 * Stores a file sent by the client. Issues a {@link ST_UNABLE_TO_WRITE} on error.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opPut($socket, $path, array $headers, $body) {
		$written = file_put_contents($this->getPathOfClientFile($socket, $path), $body);
		$this->response($socket, self::OP_STORE_FILE, ($written === false)? self::ST_UNABLE_TO_WRITE : self::ST_CREATED);
	}

	/**
	 * Sends a file to the user, as specified by $path. Issues {@link ST_REQUEST_IS_FOLDER} and {@link ST_FILE_NOT_FOUND} error codes.
	 * @todo this should be DRY, since opDelete is almost equal.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opGet($socket, $path, array $headers, $body) {
		$response_body = null;
		$headers = [];
		$full_path = $this->getPathOfClientFile($socket, $path);

		if (!file_exists($full_path))		$response_code = self::ST_FILE_NOT_FOUND;
		elseif (is_dir($full_path))			$response_code = self::ST_REQUEST_IS_FOLDER;
		elseif (!is_readable($full_path))	$response_code = self::ST_FILE_NOT_FOUND;
		else {
			$file = $this->getPathOfClientFile($socket, $path);
			$response_code = self::ST_OK;
			$response_body = file_get_contents($file);
			$headers['Bytes'] = filesize($file);
			$headers['Lines'] = null;
		}

		$this->response($socket, self::OP_GET_FILE, $response_code, $headers, $response_body);
	}

	/**
	 * Deletes a file at the client request. Issues {@link ST_REQUEST_IS_FOLDER} and {@link ST_FILE_NOT_FOUND} error codes.
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opDelete($socket, $path, array $headers, $body) {
		$full_path = $this->getPathOfClientFile($socket, $path);

		if (!file_exists($full_path))		$response_code = self::ST_FILE_NOT_FOUND;
		elseif (is_dir($full_path))			$response_code = self::ST_REQUEST_IS_FOLDER;
		elseif (!is_readable($full_path))	$response_code = self::ST_FILE_NOT_FOUND;
		else								$response_code = unlink($full_path)? self::ST_OK : self::ST_UNABLE_TO_WRITE;

		$this->response($socket, self::OP_DELETE_FILE, $response_code);
	}

	/**
	 * Should appropriately logout and disconnect the client.
	 * @param resource $socket
	 */
	abstract protected function logout($socket);

	/**
	 * Logs out the user.
	 * @see logout
	 * @param resource $socket
	 * @param string $path
	 * @param array $headers
	 * @param string $body
	 */
	protected function opExit($socket, $path, array $headers = [], $body = '') {
		$this->logout($socket);
	}

}
