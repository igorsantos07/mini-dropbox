<?
/**
 * Implements the summary functions used by the system
 * @package Traits
 */
trait SummaryFunctions {

	/** @var array
	 * The available summary functions for the given program. */
	protected $summaryOptions = [];
	/** @var string
	 * The choosen summary function name. */
	protected $summaryName;
	/** @var callable
	 * The actual summary function. */
	public $summaryFunction;

	/**
	 * Sets the {@link summaryName} and {@link summaryFunction}.
	 * @param const $name one of the `DropProtocol::SUM_*` constants.
	 * @throws UnexpectedValueException In the case when the developer forgot to specify a new summary function constant.
	 */
	public function setSummary($name) {
		switch ($name) {
			case DropProtocol::SUM_MD5_HASH:
				$this->summaryFunction = function($file) { return md5_file($file, true); };
			break;

			case DropProtocol::SUM_BYTE_COUNT:
				$this->summaryFunction = 'filesize';
			break;

			default:
				throw new UnexpectedValueException('Unknown summary function value. Maybe there\'s no specification on how it runs?');
		}
		$this->summaryName = $name;
	}

	/**
	 * Returns a binary MD5, whether the received `$md5` is binary or hexadecimal.
	 * @param mixed $md5
	 * @return string A binary representation of the MD5
	 */
	protected function ensureBinaryMD5($md5) { return (strlen($md5) == 16)? $md5 : hex2bin($md5); }
	/**
	 * Returns a hexadecimal MD5, whether the received `$md5` is binary or hexadecimal.
	 * @param mixed $md5
	 * @return string A hexadecimal representation of the MD5
	 */
	protected function ensureStringMD5($md5) { return (strlen($md5) == 32)? $md5 : bin2hex($md5); }

}