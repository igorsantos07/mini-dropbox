<?
const LF = "\r\n";
const DS = DIRECTORY_SEPARATOR;
 ini_set("auto_detect_line_endings", true); //avoiding mac/unix/windoze line-ending problems

 /**
  * Generic implementation of the Drop Protocol. Abstracts reading and writing messages, as well as Status and Operations information.
  *
  * **Available operations**
  * - `LOGIN` {@link OP_LOGIN}
  * - `EXG` {@link OP_EXCHANGE_SELECTION}
  * - `DIR` {@link OP_DIRECTORY_LISTING}
  * - `GET` {@link OP_GET_FILE}
  * - `PUT` {@link OP_STORE_FILE}
  * - `DELETE` {@link OP_DELETE_FILE}
  * - `EXIT` {@link OP_EXIT}
  *
  * **Available response codes**
  * - **2XX Success**: {@link STTYPE_SUCCESS}
  * - `200` {@link ST_OK}
  * - `201` {@link ST_CREATED}
  * - `204` {@link ST_NO_CONTENT}
  *
  * - **3XX Redirection**: {@link STTYPE_REDIRECTION}
  * - `303` {@link ST_NO_MATCH_FUNC}
  *
  * - **4XX Client error**: {@link STTYPE_CLIENT_ERROR}
  * - `401` {@link ST_BAD_LOGIN}
  * - `403` {@link ST_UNAUTHORIZED}
  * - `404` {@link ST_FILE_NOT_FOUND}
  * - `405` {@link ST_REQUEST_IS_FOLDER}
  *
  * - **5XX Server error**: {@link STTYPE_SERVER_ERROR}
  * - `501` {@link ST_NOT_IMPLEMENTED}
  * - `502` {@link ST_UNABLE_TO_WRITE}
  *
  * @package Protocol
  * @todo implement a 505 VERSION NOT SUPPORTED message somewhere
  */
abstract class DropProtocol {

	/** @var string
	 * Current version of this implementation. Also used to parse requests and responses.
	 */
	const VERSION = 'DROP/1.0';

	/** Summary function based on MD5 binary hashes. */
	const SUM_MD5_HASH		= 'MD5_hash';
	/** Summary function based on number of bytes in the file. */
	const SUM_BYTE_COUNT	= 'byte_count';

	/** Defines a message as a Request. Used by {@link readMsg}. */
	const MSG_REQUEST	= '>';
	/** Defines a message as a Response. Used by {@link readMsg}. */
	const MSG_RESPONSE	= '<';

	/** Logs in the client. The request should be sent with an `Authorization` header containing `user:password`.
	 * If the server is unable to login the client, issues a 403 response. */
	const OP_LOGIN				= 'LOGIN';
	/** Negotiates a summary function. The body of the request contains all the available summary functions,
	 * and the response contains only one, the one choosen by the server. If nothing matches, issues a 303
	 * empty response. */
	const OP_EXCHANGE_SELECTION	= 'EXG';
	/** Lists the available files. The request is empty, while the response should contain all files and their
	 * respective values for the selected summary function (by `EXG`), both separated by colons, each file starting
	 * a new line in the body. */
	const OP_DIRECTORY_LISTING	= 'DIR';
	/** The client asks for a file. The response should contain the file contents in the body.
	 * Should include the file path in the request - not the response. Issues a 404 error when the file dies not exist. */
	const OP_GET_FILE			= 'GET';
	/** The client is sending a file to the server. The request should contain the file contents in the body.
	 * Should include the file path in the request - not the response. Issues a 502 error when the server is unable to save
	 * the contents of the file. */
	const OP_STORE_FILE			= 'PUT';
	/** The client asks the server to delete a file. Should include the file path in the request - not the response. */
	const OP_DELETE_FILE		= 'DELETE';
	/**  Logs out the user from the server. */
	const OP_EXIT				= 'EXIT';
	/**  Operation specific for when the request is unintelligible. */
	const OP_ERROR				= 'ERROR';

	/** `2XX` General success codes */
	const STTYPE_SUCCESS		= 2;
	/** `Ok`: The request has succeeded. */
	const ST_OK					= 200;
	/** `Created`: The request has succeeded and the resource was created. */
	const ST_CREATED			= 201;
	/** `No Content`: The request has succeeded but there's no content to be shown. */
	const ST_NO_CONTENT			= 204;

	/** `3XX` General redirection codes */
	const STTYPE_REDIRECTION	= 3;
	/** `No matching function`: Response to a `EXG` request.
	 * The server found no matching function given the request list. The client should try again with a more permissive list. */
	const ST_NO_MATCH_FUNC		= 303;

	/** `4XX` General client error codes */
	const STTYPE_CLIENT_ERROR	= 4;
	/** `Bad Request`: The request was unintelligible. */
	const ST_BAD_REQUEST		= 400;
	/** `Bad login`: The client supplied a bad combination of user and password in the `Authorization` header of a `LOGIN` request. */
	const ST_BAD_LOGIN			= 401;
	/** `Unauthorized`: The client is not allowed to do the requested operation without being authorized first. */
	const ST_UNAUTHORIZED		= 403;
	/** `File not found`: The server did not find the file requested by the client through `GET`. */
	const ST_FILE_NOT_FOUND		= 404;
	/** `Request is a folder`: The `GET` requested path is, in fact, a folder, and thus should not be returned by the server. */
	const ST_REQUEST_IS_FOLDER	= 405;

	/** `5XX` General server error codes */
	const STTYPE_SERVER_ERROR	= 5;
	/** `Not implemented`: The server understood your request message but has no idea of what do to with it;
	 * the requested operation is probably not implemented. */
	const ST_NOT_IMPLEMENTED	= 501;
	/** `Unable to write file`: The server was not able to write a file. Usually a response to `PUT`. */
	const ST_UNABLE_TO_WRITE	= 502;

	/** @var string
	 * Directory used to store the files to be synced. */
	protected $dir;
	/** @var int
	 * Port used for connection. */
	protected $port;
	/** @var string
	 * Preferred summary function by the program. */
	protected $preferredSummary;

	/**
	 * Starts the Protocol client.
	 * @param array $summaryOptions The Summary Functions that are available for usage. List of DropProtocol::SUM_*.
	 */
	public function __construct(array $summaryOptions = []) {
		$this->summaryOptions = $summaryOptions;
	}

	/**
	 * Should print bare stuff.
	 * @param string $sentence
	 */
	abstract public function debugRaw($sentence);
	/**
	 * Should print organized stuff.
	 * @param string $sentence
	 */
	abstract public function debugMsg($sentence);
	/**
	 * Should print formatted stuff.
	 * @param mixed $msg A message or an array of messages.
	 */
	abstract public function debugOutput($msg);

	/**
	 * Receives one of the `ST_*` constants and returns the textual version of the response code.
	 * If the response code is not identified, returns `???` (three interrogation marks).
	 * @param const $code
	 * @return string
	 */
	static protected function getStatus($code) {
		switch ($code) {
			/* ********* 2XX success ********** */
			case self::ST_OK:				return 'Ok';
			case self::ST_CREATED:			return 'Created';
			case self::ST_NO_CONTENT:		return 'No Content';

			/* ******* 2XX redirection ******** */
			case self::ST_NO_MATCH_FUNC:	return 'No matching function';

			/* ******* 4XX client error ******* */
			case self::ST_BAD_REQUEST:			return 'Bad Request';
			case self::ST_BAD_LOGIN:			return 'Bad login';
			case self::ST_UNAUTHORIZED:		return 'Unauthorized';
			case self::ST_FILE_NOT_FOUND:		return 'File not found';
			case self::ST_REQUEST_IS_FOLDER:	return 'Request is a folder';

			/* ******* 5XX server error ******* */
			case self::ST_NOT_IMPLEMENTED:	return 'Not Implemented';
			case self::ST_UNABLE_TO_WRITE:	return 'Unable to write file';

			default: return '???';
		}
	}

	/**
	 * Tells whether a given `$operation` needs authentication to be run.
	 * @param const $operation
	 * @return boolean
	 */
	static public function needAuthentication($operation) {
		switch ($operation) {
			case self::OP_LOGIN:
			case self::OP_EXIT:
				return false;
			break;

			case self::OP_DELETE_FILE:
			case self::OP_DIRECTORY_LISTING:
			case self::OP_EXCHANGE_SELECTION:
			case self::OP_GET_FILE:
			case self::OP_STORE_FILE:
			default:
				return true;
			break;
		}
	}

	/**
	 * Tells whether a $value is one of the `OP_*` constants or not.
	 * @todo should use get_defined_constants() to avoid having to define a constant twice (here and in the class header)
	 * @param mixed $value
	 * @return boolean
	 */
	static protected function isOperation($value) {
		return in_array($value, [
			self::OP_DELETE_FILE,
			self::OP_DIRECTORY_LISTING,
			self::OP_EXCHANGE_SELECTION,
			self::OP_EXIT,
			self::OP_GET_FILE,
			self::OP_LOGIN,
			self::OP_STORE_FILE
		]);
	}

	/**
	 * Sends a message through the $socket.
	 * @param resource $socket the socket to be used for communication.
	 * @param string $first_line The first line of the request, that specifies if this is a request or a response.
	 * @param array $headers Any header that should accompany the request/response.
	 * @param mixed $data Any that should go in the body.
	 * @param boolean $file If this message contains a file. Will take additional care to not mess up with the file contents
	 * nor mess with debug information outputing binary stuff to it.
	 */
	protected function sendMsg($socket, $first_line, array $headers = [], $data = null, $file = false) {
		$msg = $first_line.LF;

		if ($data) {
			if (!array_key_exists('Bytes', $headers)) $headers['Bytes'] = mb_strlen($data);
			if (!array_key_exists('Lines', $headers)) {
				$headers['Lines'] = substr_count($data, LF);
				if (substr($data, -2, 2) != LF) ++$headers['Lines'];
			}
		}
		$headers = ArrayHelper::clear($headers, true);

		foreach ($headers as $header => $value)
			$msg .= ucfirst($header).': '.$value.LF;
		$msg .= LF;
		$debug_msg = $msg;

		if ($data) $msg .= $data.((!$file && substr($data, -2, 2) != LF)? LF : '');

		if (!$file) $debug_msg = $msg;
		else $debug_msg .= "TRUNCATED BODY: FILE CONTENTS IS PRESENT\n";
		$this->debugRaw('Sending:');
		$this->debugOutput($debug_msg);

		socket_write($socket, $msg);
	}

	/**
	 * Tries to read one part of the message, being it the header OR the body.
	 * Uses {@link Sockets::socketError()} and thus tries to close connection if there's an error.
	 * @param resource $read The socket with messages
	 * @return string The header OR body
	 */
	private function readPart($read, $lines = true, $bytes = 1024) {
		$part = '';
		$read_type = ($lines > 0 || $lines === true)? PHP_NORMAL_READ : PHP_BINARY_READ; // normal read goes until the first \r or \n

		$this->debugMsg('Going to do '.($read_type == PHP_NORMAL_READ? 'TEXT' : 'BINARY')." read of {$bytes}B and ".(is_numeric($lines)? "$lines lines" : "auto lines"));

		if ($read_type == PHP_BINARY_READ) {
			do {
				$this->debugRaw('.');
				$part .= @socket_read($read, $bytes, $read_type);
				$total_read = strlen($part);
			}
			while ($total_read < $bytes);
			$this->debugOutput("TRUNCATED. FILE CONTENTS PRESENT, TOTAL {$total_read}B \n");
		}
		else {
			$read_lines = 0;
			while ($tmp = @socket_read($read, $bytes, $read_type)) {
				$this->debugRaw('.');
				while (substr($tmp, -1, 1) != "\n")
					$tmp .= @socket_read($read, 1, PHP_NORMAL_READ);

				$part .= $tmp;

				if ($tmp == LF || $tmp == '' || (is_numeric($lines) && ++$read_lines == $lines)) break;
			}
			$this->debugOutput($part);
		}

		$this->socketError($read);

		return $part;
	}

	/**
	 * Reads a message from the socket.
	 * @param resource $socket The socket to be read from
	 * @param string $type One of the `MSG_*` constants, meaning if this is to read a request or a response
	 * @return array An associative array with the following elements:
	 *   **header_content**: raw header;
	 *   **head**: associative array with head information such as operation and actual headers;
	 *   **body**: raw body.
	 * @see DropProtocol::readRequest()
	 * @see DropProtocol::readResponse()
	 */
	protected function readMsg($socket, $type) {
		$this->debugMsg('Getting header:');
		$header_content = $this->readPart($socket);

		if ($header_content) {
			$splitFunc = ($type == self::MSG_REQUEST)? 'splitRequestHeaders' : 'splitResponseHeaders';
			$head = self::$splitFunc($header_content);

			if (isset($head['headers']['Bytes']) || isset($head['headers']['Lines'])) {
				$this->debugMsg('Getting body:');
				$body = $this->readPart($socket, isset($head['headers']['Lines'])? $head['headers']['Lines'] : false, $head['headers']['Bytes']);
			}
			else
				$body = '';

			return compact('header_content', 'head', 'body');
		}
		else {
			return ['header_content' => '', 'head' => '', 'body' => ''];
		}
	}

	/**
	 * An alias for {@link readMsg} with the $type set to {@link MSG_REQUEST}.
	 * @param resource $read the socket to be read from
	 * @return array an associative array as specified by {@link readMsg()}.
	 * @see DropProtocol::readResponse()
	 */
	protected function readRequest($read) { return $this->readMsg($read, self::MSG_REQUEST); }
	/**
	 * An alias for {@link readMsg} with the $type set to {@link MSG_RESPONSE}.
	 * @param resource $read the socket to be read from
	 * @return array an associative array as specified by {@link readMsg()}.
	 * @see DropProtocol::readRequest()
	 */
	protected function readResponse($read) { return $this->readMsg($read, self::MSG_RESPONSE); }

	/**
	 * Splits the header content into the first line and the headers.
	 * @param type $head
	 * @return array an associative array as follows:
	 *  **first_line**: the first line, that should be further interpreted;
	 *  **headers**: associative array with the header name and header value.
	 */
	static private function splitHeaders($head) {
		$lines = ArrayHelper::clear(explode(LF, $head));
		$first_line = array_shift($lines);

		$headers = [];
		foreach($lines as $head) {
			$header_name = strtok($head, ':');
			$headers[trim($header_name)] = trim(strtok(''));
		}

		return compact('first_line', 'headers');
	}

	/**
	 * Splits a Request header into meaningful information.
	 * @param string $head The header content.
	 * @return array an associative array as follows:
	 *   **operation**: the operation in the request;
	 *   **path**: the path received in the request;
	 *   **headers**: associative array with the header name and header value.
	 */
	static protected function splitRequestHeaders($head) {
		extract(self::splitHeaders($head));

		$operation = $path = null;
		$first_line = explode(' ', $first_line);
		$total_elements = sizeof($first_line);
		switch ($total_elements) {
			case 3: //with path
				$path = $first_line[1];
			case 2: //no path
				$operation = $first_line[0];
			break;
		}

		return compact('operation','path','headers');
	}

	/**
	 * Splits a Response header into meaningful information.
	 * @param string $head The header content.
	 * @return array an associative array as follows:
	 *   **operation**: the operation in the response;
	 *   **status_code**: the response code received;
	 *   **status_txt**: the response message received;;
	 *   **headers**: associative array with the header name and header value.
	 */
	static protected function splitResponseHeaders($head) {
		extract(self::splitHeaders($head));

		$version		= strtok($first_line, ' ');
		$operation		= strtok(' ');
		$status_code	= strtok(' ');
		$status_txt		= strtok('');

		return compact('operation','status_code','status_txt','headers');
	}

}
