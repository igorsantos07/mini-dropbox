<?
require_once 'lib/ArrayHelper.php';

/**
 * Useful actions with sockets.
 * @package Traits
 */
trait Sockets {
	/**
	 * Shows the error message for a socket in a nice way and dies.
	 * @param resource $socket
	 * @param array [optional] List of sockets available. If the connection is dropped, the given $socket will be removed from here.
	 */
	static function socketError($socket, &$list = []) {
		$errno = ($socket === false)? socket_last_error() : socket_last_error($socket);

		switch ($errno) {
			case 0:
				//no error!
			break;

			case SOCKET_EPIPE:			//broken pipe
			case SOCKET_ECONNRESET:		//connection reset
				if (is_resource($socket))
					socket_close($socket);
				if (sizeof($list) > 0)
					ArrayHelper::unsetByValue ($list, $socket);
			break;

			case SOCKET_EINPROGRESS: //timed out, the message that we receive is "operation now in progress", however
				die("\nFail! [] Could not connect, timed out after ".TIMEOUT." secs\n");
			break;


			default:
				die("\nFail! [$errno] ".socket_strerror($errno)."\n");
		}
	}
}