<?
require 'lib/pharse/pharse.php';

/**
 * Implements varied operations related to command-line interface and file management.
 * @package Traits
 * @see Pharse
 */
trait CliAndFiles {
	/** @var string
	 * Message used in the `help` command */
	private $banner = 'Implementation of Dalhousie\'s CSCI 3171 Assignment #3, on Winter 2012/2013.';

	/** @var boolean
	 * Tells whether the application is in debug mode */
	public $debug = false;

	/** @var array
	 * Default command-line options, used by all systems that use this trait.
	 * Should be in the same format accepted by {@link Pharse}, specified at {@link PharseOption}. */
	private $defaultOptions = [
		'dir' => [
			'description'	=> 'the directory that the program will use for synchronization',
			'default'		=> 'files/',
			'type'			=> 'string',
			'required'		=> false,
			'short'			=> 'd',
		],
		'port' => [
			'description'	=> 'port on which this program will interact',
			'default'		=> 19905,
			'type'			=> 'int',
			'required'		=> false,
			'short'			=> 'p',
		],
		'summary' => [
			'description'	=> 'Summary funtion preferred, between "MD5_hash" and "byte_count". Uses all possible values if nothing is supplied.',
			'type'			=> 'string',
			'required'		=> false,
			'short'			=> 's',
		],
		'debug' => [
			'description'	=> 'Puts the system in debug mode, outputing a lot of stuff',
			'short'			=> 'debug',
		],
		'help' => [
			'description'	=> 'show this help',
			'short'			=> 'help',
		]
	];

	/** @var array
	 * Final options, after adding any new stuff with {@link addOptions} */
	private $finalOptions = [];
	/** @var array
	 * Anything gathered from the options when starting the application.
	 * Also includes keys in the form "{$option}_given" with boolean values, for thecase when the option was given with no value. */
	private $params = [];

	/**
	 * Sets the banner used by the `help` command.
	 * @param string $banner
	 */
	public function setBanner($banner) {
		$this->banner = $banner;
	}

	/**
	 * Includes an array of options to be used by the command-line interface of this program.
	 * @param array $additionalOptions Options in the format accepted by {@link Pharse}, specified at {@link PharseOption}.
	 */
	public function addOptions(array $additionalOptions) {
		$this->finalOptions = array_merge($this->defaultOptions, $additionalOptions);
		ksort($this->finalOptions);
	}

	/**
	 * Mount the finalOptions array and interprets the options given, filling up them in {@link params}.
	 * Also outputs an alert if the `debug` option was given.
	 */
	public function handleCliOptions() {
		if (!sizeof($this->finalOptions))
			$this->finalOptions = $this->defaultOptions;

		Pharse::setBanner($this->banner);
		$this->params = Pharse::options($this->finalOptions);

		if (isset($this->params['debug_given'])) {
			$this->debug = true;
			echo "System started in DEBUG MODE!\n\n";
		}
	}

	/**
	 * Looks for a value given in the script params for the given option; if there's nothing there,
	 * tries to get a default value. If there's no default too, returns an empty string.
	 * @param string $option the option you want the value for
	 * @return string the value for the asked option, or empty if there's nothing
	 * @throws UnexpectedValueException in case the option asked does not exist
	 */
	public function getParam($option) {
		$options = (sizeof($this->finalOptions))? $this->finalOptions : $this->defaultOptions;
		if (array_key_exists($option, $options)) {
			if (isset($this->params["{$option}_given"])) {
				if (strlen($this->params[$option]) > 0)
					return $this->params[$option];
				elseif (array_key_exists('default', $options[$option]))
					return $options[$option]['default'];
				else
					return true;
			}
			else {
				if (array_key_exists('default', $options[$option]))
					return $options[$option]['default'];
				else
					return null;
			}
		}
		else {
			throw new UnexpectedValueException("There's no option '$option'.");
		}
	}

	/**
	 * Tells whether a directory exists and is writable.
	 * @param string $dir path to the given directory
	 * @return boolean
	 */
	static public function verifyDir($dir) {
		if (!is_dir($dir))			return "The given 'dir' ($dir) does not look like a directory.\n";
		elseif (!is_writable($dir))	return "The given 'dir' ($dir) is not writable.\n";
		else return true;
	}

	/**
	 * Gives the current state of files in the given `$dir`, using the appropriate `$summaryFunction`.
	 * @param string $dir path to the given directory
	 * @param callable $summaryFunction
	 * @return array
	 */
	static public function getLocalFileState($dir, callable $summaryFunction) {
		$files = [];
		foreach(scandir($dir) as $file) {
			$complete_path = $dir.DIRECTORY_SEPARATOR.$file;
			if (is_dir($complete_path)) continue; //we don't need to implement sub-folders
			$files[$file] = $summaryFunction($complete_path);
		}
		return $files;
	}

	/**
	 * Outputs raw stuff.
	 * @param string $sentence
	 */
	public function debugRaw($sentence) {
		if ($this->debug) echo $sentence;
	}

	/**
	 * Outputs something with an arrow pointing (to show that it is a debug message) and a line break.
	 * @param string $sentence
	 */
	public function debugMsg($sentence) {
		if ($this->debug) echo "==> $sentence\n";
	}

	/**
	 * Nicely outputs text, using a signal (`tilde ~`) to separate its parts.
	 * If it receives an array as argument, each value will be fenced by signals. If it's a simple string,
	 * it will still be fenced by the same signals.
	 * @param string $msg
	 */
	public function debugOutput($msg) {
		if ($this->debug) {
			if (is_array($msg)) {
				echo "\n~~~~~~~\n";
				if (sizeof($msg) > 0) {
					foreach ($msg as $part) {
						echo strtr($part, ["\r\n" => '\r\n'."\n", "\r" => '\r', "\n" => '\n'."\n"])."~~~~~~~\n";
					}
					echo "\n";
				}
				else {
					echo "~~~~~~~\n";
				}
			}
			else {
				echo "\n~~~~~~~\n".strtr($msg, ["\r\n" => '\r\n'."\n", "\r" => '\r', "\n" => '\n'."\n"])."~~~~~~~\n";
			}
		}
	}
}