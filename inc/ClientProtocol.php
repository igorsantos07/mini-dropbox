<?
require 'DropProtocol.php';
require 'SummaryFunctions.php';

/**
 * Implementation of the  Protocol-related issues in the {@link Client} side. This class handles everythjng related to the Protocol and its operations.
 * @package Protocol
 *
 * @method mixed opDelete(string $file) Requests the server to remove $file.
 * @method mixed opGet(string $file) Requests from the server the contents of $file and saves it.
 * @method mixed opPut(string $complete_path) Send the file in the $complete_path to the server.
 *
 * @method mixed opLogin(string $user, string $password, array $handlers) Logs the $user with a $password into the server. Handles the response with the $handlers functions. See details at {@link request}.
 *
 * @method mixed opExg(array $handlers) Negotiates a Summary Function with the server and handles the response with the $handlers functions. See details at {@link request}.
 * @method mixed opDir(array $handlers) Asks the server a list of files with the negotiated Summary Function, and handles the response with the $handlers functions. See details at {@link request}.
 * @method mixed opExit(array $handlers) Logs out of the server and handles the response with the $handlers functions. See details at {@link request}.
 */
abstract class ClientProtocol extends DropProtocol {

	use SummaryFunctions;

	/** @var resource
	 * The socket uaed to communicate with the server */
	protected $socket;
	/** @var string
	 * Name of the file used for logging the last sync status */
	protected $data_file;
	/** @var string
	 * Folder choosen for synchonization */
	protected $dir;

	/**
	 * Should create a connection with the server and store it under {@link ClientProtocol::socket}
	 */
	abstract protected function connect();
	/**
	 * Should gracefully close the connection<=, optionally show a $msg and exit.
	 * @todo could also proper issue exit codes.
	 */
	abstract protected function disconnect($msg = '');
	/**
	 * Should return a response from the server in an associative array as given by {@link readResponse} and thus {@link readMsg}.
	 */
	abstract protected function getResponse();

	/**
	 * Starts the Protocol client.
	 * @param array $summaryOptions The Summary Functions that are available for usage. List of DropProtocol::SUM_*.
	 */
	public function __construct(array $summaryOptions = []) {
		parent::__construct($summaryOptions);
	}

	/**
	 * Used to implement the several different operations.
	 * As they are all similar and pretty much small, it's handy to have then wrapped in the same place. It's basically a wrapper for {@link ClientProtocol::request()}.
	 * @param string $name name of the called method
	 * @param array $arg indexed array of arguments received by the given method
	 */
	public function __call($name, $arg) {
		$operation = strtoupper(substr($name, 2));
		if (!(substr($name, 0, 2) == 'op' && self::isOperation($operation)))
			throw new BadMethodCallException("Unknown method $name");

		switch ($operation) {
			case self::OP_DELETE_FILE:
				return $this->request($operation, ['path' => basename($arg[0])]);
			break;

			case self::OP_GET_FILE:
				$path = basename($arg[0]);
				return $this->request($operation, ['path' => $path], [
					self::ST_OK => function($resp) use ($path) { file_put_contents($this->dir.DS.$path, $resp['body']); }
				]);
			break;

			case self::OP_LOGIN:
				return $this->request($operation, ['headers' => ['Authorization' => "$arg[0]:$arg[1]"]], $arg[2]);
			break;

			case self::OP_EXCHANGE_SELECTION:
				return $this->request($operation, ['body' => basename($arg[0])], $arg[1]);
			break;

			case self::OP_DIRECTORY_LISTING:
			case self::OP_EXIT:
				return $this->request($operation, [], $arg[0]);
			break;

			case self::OP_STORE_FILE:
				return $this->request($operation, [
					'path' => basename($arg[0]),
					'headers' => ['Bytes' => filesize($arg[0]), 'Lines' => null],
					'body' => file_get_contents($arg[0])
				]);
			break;

			default: return $this->request($operation, $arg[0], $arg[1]);
		}
	}

	/**
	 * Fires a request given the parameters and handles a response based on the $handlers functions. There are
	 * default handler functions for OK and unknown status codes: the first one simply issues a debug message,
	 * and the second calls {@link self::disconnect()} with the response status and message.
	 *
	 * @param string $operation one of the DropProtocol::OP_* constants
	 * @param mixed $info [optional] an associative array with information or the actual body of the request.
	 *  If an array, it can have any of the following keys:
	 *  - path: the path to be requested
	 *  - headers: an associative array of headers (no need of colons!)
	 *  - body: the request body to be sent
	 * @param array $handlers An array with keys being status codes and values being functions that will actually
	 * do stuff with the response, that's sent as a parameter to the function. This parameter is an associative
	 * array as given by {@link getResponse()}. If the handler function returns a value, this value is passed
	 * along by this method. There are some special keys that can also be used:
	 *  - success: used for all 2XX statuses that have no specific handler
	 *  - default: overrides the "unknown code" handler
	 *  - empty: used when the response is empty
	 *
	 * @return whatever the handler function returns, or null if there's no return value or it called a default return function.
	 */
	protected function request($operation, $info = '', array $handlers = []) {
		if (is_array($info)) {
			$path		= (isset($info['path']) && !empty($info['path']))? $info['path'] : '';
			$body		= (isset($info['body']) && !empty($info['body']))? $info['body'] : '';
			$headers	= (isset($info['headers']) && is_array($info['headers']))? $info['headers'] : [];
		}
		else {
			$path = '';
			$body = $info;
			$headers = [];
		}

		$request_line = $operation.' '.($path? $path.' ' : '').self::VERSION;
		$this->sendMsg($this->socket, $request_line, $headers, $body, $operation == self::OP_STORE_FILE);

		$response = $this->getResponse();
		if (!$response['header_content']) {
			if (array_key_exists('empty', $handlers))
				$handlers['empty']($response);
			else
				$this->disconnect('Server sent an empty response. Exiting.');
		}
		$status_code = $response['head']['status_code'];
		$returned = null;

		switch (substr($status_code, 0, 1)) {
			case self::STTYPE_SUCCESS:
				if (array_key_exists($status_code, $handlers))
					$returned = $handlers[$status_code]($response);
				elseif (array_key_exists('success', $handlers))
					$returned = $handlers['success']($response);
				else
					$this->debugMsg("Server is fine: \"{$response['head']['status_txt']}\"\n");
			break;

			default:
				if (array_key_exists($status_code, $handlers))
					$returned = $handlers[$status_code]($response);
				else {
					if (array_key_exists('default', $handlers))
						$returned = $handlers['default']($response);
					else
						$this->disconnect("Weird response: [$status_code] \"{$response['head']['status_txt']}\"");
				}
		}

		return $returned;
	}

	/**
	 * Tries to login in the server, using the provided {@link $user} and {@link password}.
	 * Disconnects if receives a {@link ST_BAD_LOGIN} response.
	 */
	public function login() {
		$this->opLogin($this->user, $this->password, [
			self::ST_BAD_LOGIN => function() {
				$this->disconnect("Oops. Bad login. Did you type user/password correctly?");
			}
		]);
	}

	/**
	 * Negotiates a summary function with the server.
	 * Disconnects if receives a {@link ST_NO_MATCH_FUNC} response.
	 * @todo should try to resend the negotiation request for a couple of times before giving up
	 */
	public function negotiateSummary() {
		$options = (is_callable($this->summaryFunction))? [$this->summaryName] : $this->summaryOptions;

		$this->opExg(implode(LF, $options).LF, [
			self::ST_OK => function($response) {
				$this->setSummary(trim($response['body']));
			},
			self::ST_NO_MATCH_FUNC => function() {
				$this->disconnect("Not able to define a common summary function... I'm giving up, the server is too boring :(");
			},
		]);
	}

	/**
	 * Issues a DIR command and then syncs stuff with the server.
	 */
	public function syncFiles() {
		$server_files	= $this->getServerState();
		$log_files		= $this->getLastState();
		$local_files	= CliAndFiles::getLocalFileState($this->dir, $this->summaryFunction);
		$server_total	= sizeof($server_files);
		$log_total		= sizeof($log_files);

		if (!$server_total && !$log_total) { //never synced before!
			foreach ($local_files as $file => $summary)
				$this->opPut($this->dir.DS.$file);
		}
		else {
			if ($this->summaryName == self::SUM_MD5_HASH)
				$server_files = array_map ('self::ensureBinaryMD5', $server_files);

			/*
			 * There's nine possible states of the files:
			 * 1. same on the three places: ignored, nothing to do
			 * 2. same on client and server, different on log: file created equally at both ends, nothing to do.
			 * 3. same on client and log, different on server: file was synced once but changed in the server; should be downloaded.
			 * 4. same on server and log, different on client: file was synced once but changed in the client; should be uploaded.
			 * 5. same on client and log, non-existent on server: file was synced once but deleted on the server; should be deleted locally.
			 * 6. same on server and log, non-existent on client: file was synced once but deleted on the server; should be deleted remotely.
			 * 7. non-existent on client and log, present on server: file never existed, but sent by someone else to the server; should be downloaded.
			 * 8. non-existent on server and log, present on client: file never synced, but created locally; should be uploaded.
			 * 9. different on the three places: not covered by the assignment. (probably would need to use timestamp verification).
			 */

			//ignoring unchanged files, cases 1 and 2
			$unchanged_files = array_intersect_assoc($server_files, $local_files);

			//merging all changed files into one array with the summary functions of all places
			$files_to_check = [];
			foreach(['server', 'log', 'local'] as $place) {
				foreach (${$place.'_files'} as $file => $summary) {
					if (array_key_exists($file, $unchanged_files)) continue;
					elseif (isset($files_to_check[$file])) $files_to_check[$file][$place] = $summary;
					else $files_to_check[$file] = [$place => $summary];
				}
			}

			$three_ways = array_filter($files_to_check, function($file) { return sizeof($file) == 3; });
			foreach($three_ways as $file => $summaries) { //cases 3, 4 and 9
				/*9*/ if ($summaries['log'] != $summaries['local'] && $summaries['local'] != $summaries['log']) $this->handleThreeWayChanges();
				/*3*/ if ($summaries['log'] != $summaries['server'])		$this->opGet($file);
				/*4*/ elseif ($summaries['log'] != $summaries['local'])		$this->opPut($this->dir.DS.$file);
			}

			$two_ways = array_filter($files_to_check, function($file) { return sizeof($file) == 2; });
			foreach($two_ways as $file => $summaries) { //cases 5 and 6
				/*5*/ if (!isset($summaries['server']))		unlink($this->dir.DS.$file);
				/*6*/ elseif (!isset($summaries['local']))		$this->opDelete($file);
			}

			$one_way = array_filter($files_to_check, function($file) { return sizeof($file) == 1; });
			foreach($one_way as $file => $summaries) { //cases 7 and 8
				/*7*/ if (isset($summaries['server']))		$this->opGet($file, $handlers['get']);
				/*8*/ elseif (isset($summaries['local']))	$this->opPut($this->dir.DS.$file);
			}
		}

		$this->updateLastState();
	}

	/**
	 * Issues an EXIT command to the server.
	 * Should not be used directly! To disconnect you should call {@link disconnect}.
	 */
	protected function goodbye() {
		return $this->opExit([
			self::ST_OK	=> function() { return true; },
			//had to override the default handler to avoid an infinite loop of disconnections
			'default'	=> function($resp) { return sprintf('[%d] %s', $resp['head']['status_code'], $resp['head']['status_txt']); }
		]);
	}

	/**
	 * For now, it sends to the limbo. Should probably use some sort of timestamp comparison to see which side is more recent.
	 * @return null
	 */
	private function handleThreeWayChanges() {
		return null;
	}

	/**
	  * Issues a DIR command.
	  * @return array An associative array with the keys being the file names and values, the summary function result.
	  */
	private function getServerState() {
		return $this->opDir([
			self::ST_NO_CONTENT => function() { return []; },
			self::ST_OK => function($resp) {
				$files = [];
				$body = ArrayHelper::clear(explode(LF, $resp['body']), false);
				foreach ($body as $file)
					$files[strtok($file, ':')] = strtok('');
				return $files;
			}
		]);
	}

	/**
	 * Reads the data file with the results of the last sync using the given summary function.
	 * @return array An associative array with the keys being the file names and values, the summary function result.
	 * @see ClientProtocol::getDataFile()
	 */
	private function getLastState() {
		return (unserialize(file_get_contents($this->getDataFile())))?: [];
	}

	/**
	 * Writes in the datafile the current state of files inside the user folder. Uses `serialize()` to store the information array.
	 * @see CliAndFiles::getLocalFileState()
	 * @todo Could be optimized using already generated summaries for unchanged files.
	 */
	private function updateLastState() {
		$choosen_summary = $this->summaryName;
		foreach ($this->summaryOptions as $summary) {
			$this->setSummary($summary);
			$current_files = CliAndFiles::getLocalFileState($this->dir, $this->summaryFunction);
			file_put_contents($this->getDataFile(), serialize($current_files));
		}
		$this->setSummary($choosen_summary);
	}

	/**
	 * Returns the data file used to store the last synchronization status.
	 * @return string path to a file.
	 */
	protected function getDataFile() {
		$file = implode(DS, ['files', 'client', 'states', rtrim(strtr("$this->summaryName-$this->dir", '\/', '--'), '-')]);
		if (!file_exists($file)) touch($file);
		return $file;
	}

}