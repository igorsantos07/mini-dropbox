#!/usr/bin/env php
<?
require 'inc/ClientProtocol.php';
require 'inc/CliAndFiles.php';
require 'inc/Sockets.php';

/**
 * Implements the main client.
 * @package Application
 */
class Client extends ClientProtocol {
	use CliAndFiles, Sockets;

	/** @var int
	 * Time to wait for a server response before giving up. */
	const TIMEOUT = 10;

	/** @var string
	 * The host to connect with. */
	private $host;

	/** @var string
	 * The username to use when loging in the server. */
	protected $user;
	/** @var string
	 * The password to use when loging in the server. */
	protected $password;

	/**
	 * Read the CLI options and starts the server.
	 * @param array $summaryOptions The Summary Functions that are available for usage. List of DropProtocol::SUM_*.
	 */
	public function __construct(array $summaryOptions = []) {
		parent::__construct($summaryOptions);

		$this->setBanner("Client of Dalhousie's CSCI 3171 Assignment #3, on Winter 2012/2013.");
		$this->addOptions([
			'host' => [
				'description'	=> 'host with which this program will interact',
				'default'		=> 'localhost',
				'type'			=> 'string',
				'required'		=> false,
				'short'			=> 'h',
			],
			'user' => [
				'description'	=> '[required] the username used to synchronize',
				'type'			=> 'string',
				'required'		=> true,
				'short'			=> 'u',
			],
			'password' => [
				'description'	=> '[required] the password used to synchronize',
				'required'		=> true,
				'short'			=> 'q',
			],
		]);
		$this->handleCliOptions();

		foreach(['host', 'port', 'user', 'password', 'dir', 'summary' => 'preferredSummary'] as $name => $field)
			$this->$field = $this->getParam((is_string($name)? $name : $field));

		try {
			if ($this->preferredSummary) $this->setSummary($this->preferredSummary);
		}
		catch(UnexpectedValueException $e) {
			die($e->getMessage()."\n");
		}

		$dir_ok = CliAndFiles::verifyDir($this->dir);
		if ($dir_ok !== true) exit($dir_ok);
	}

	/**
	 * Creates a socket and connects to the server.
	 */
	public function connect() {
		$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->socket === false) {
			$errno = socket_last_error($this->socket);
			die("Could not create socket: [$errno]".socket_strerror(socket_last_error($this->socket))."\n");
		}
		else {
			echo "Attempting to connect to $this->host:$this->port... ";
			socket_set_option($this->socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => self::TIMEOUT, 'usec' => 0]);
			$result = @socket_connect($this->socket, $this->host, $this->port); //silences the warning in case of error. We are going to say it nicely later
			if ($result === false)
				self::socketError($this->socket);
			else
				echo "Connected!\n";
		}
	}

	/**
	 * Blocks until the server replies something meaningful.
	 * @return array An associative array as given by {@link readResponse} and thus {@link readMsg}.
	 */
	protected function getResponse() {
		if (!is_resource($this->socket)) exit("The connection apparently has been dropped.\n");
		if (socket_select($read = [$this->socket], $write = [], $except = [], self::TIMEOUT)) {
			return $this->readResponse($this->socket);
		}
		else {
			$this->disconnect("Server did not replied, connection timed out.");
		}
	}

	/**
	 * Closes the connection, optionally outputing a reason $msg.
	 * @param string $msg Reason to disconnect.
	 * @see goodbye
	 */
	public function disconnect($msg = '') {
		if ($msg) echo "$msg\n";
		if ($this->socket) {
			if (($message = $this->goodbye()) === true)
				echo 'Logged out successfully. ';
			else
				echo "Could not logout: $message\n";

			echo 'Closing socket... ';
			socket_close($this->socket);
		}
		echo "Closed.\n";
		exit;
	}
}

$client = new Client([Client::SUM_MD5_HASH, Client::SUM_BYTE_COUNT]);
$client->connect();
$client->login();
$client->negotiateSummary();
$client->syncFiles();
$client->disconnect();