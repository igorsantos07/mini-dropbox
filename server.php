#!/usr/bin/env php
<?
require_once 'lib/ArrayHelper.php';
require 'inc/ServerProtocol.php';
require 'inc/CliAndFiles.php';
require 'inc/Sockets.php';

/**
 * Implements the main server.
 * @package Application
 */
class Server extends ServerProtocol {
	use CliAndFiles, Sockets;

	/** @var int
	 * Number of clients to stay in the line */
	const QUEUE = 10;

	/** @var string
	 * Name of the SQLite database file */
	const DB_FILE = 'users.db';

	/** @var resource
	 * The socket used to receive new connections. */
	private $mainSocket;
	/** @var ClientMachine[]
	 * Array of connected clients. */
	private $clients = [];

	/** @var PDO
	 * The database connection. */
	private $db;

	/**
	 * Read the CLI options and starts the server.
	 * @param array $summaryOptions The Summary Functions that are available for usage. List of `DropProtocol::SUM_*`.
	 */
	public function __construct(array $summaryOptions = []) {
		parent::__construct($summaryOptions);
		$this->setBanner("Server of Dalhousie's CSCI 3171 Assignment #3, on Winter 2012/2013.");
		$this->handleCliOptions();

		foreach(['port', 'dir', 'summary' => 'preferredSummary'] as $name => $field)
			$this->$field = $this->getParam((is_string($name)? $name : $field));

		$dir_ok = CliAndFiles::verifyDir($this->dir);
		if ($dir_ok !== true) exit($dir_ok);

		$this->connectDB();
	}

	/**
	 * Creates the database connection in {@link db}.1
	 * @throws UnexpectedValueException When the {@link DB_FILE} was not found.
	 */
	private function connectDB() {
		if (!file_exists(self::DB_FILE))
			throw new UnexpectedValueException("There's no '".self::DB_FILE."' in the folder root. How would I login the guys??");
		else {
			$this->db = new PDO('sqlite:'.self::DB_FILE);
			$this->debugMsg('Connected to DB '.self::DB_FILE);
		}
	}

	/**
	 * Logs the user in, using the database connection {@link db}, and issues a {@link ST_OK} response if
	 * the credentials are right, or a {@link ST_BAD_LOGIN} if not.
	 * @param resource $socket The client socket.
	 * @param string $user User name.
	 * @param string $password User password.
	 */
	protected function login($socket, $user, $password) {
		$query = $this->db->prepare('SELECT COUNT(*) FROM users WHERE name = :name AND password = :password');
		$query->execute([':name' => $user, ':password' => $password]);
		$found = (bool) $query->fetch(PDO::FETCH_NUM)[0];

		$this->getClientFromSocket($socket)->login($user);
		$this->response($socket, self::OP_LOGIN, ($found)? self::ST_OK : self::ST_BAD_LOGIN);
	}

	/**
	 * Logs out the client, removing it from the {@link clients} array.
	 * @param resource $socket The client socket.
	 * @see closeClient
	 */
	protected function logout($socket) {
		$this->closeClient(
			$this->getClientFromSocket($socket),
			function() use ($socket) { $this->response($socket, self::OP_EXIT, self::ST_OK); }
		);
		echo "Client disconnected.\n";
	}

	/**
	 * Opens an `AF_INET` socket in {@link mainSocket} and starts listening to new connections on all local interfaces on {@link $port}.
	 * @see listeningLoop
	 */
	public function listen() {
		//creates, binds and listen on all local interfaces on the given port with an AF_INET socket
		if (($this->mainSocket = @socket_create_listen($this->port, self::QUEUE)) === false)
			self::socketError($this->mainSocket);
		else
			echo "Listening locally at port $this->port...\n\n";

		$this->listeningLoop();
	}

	/**
	 * Infinite loop listening for information on {@link mainSocket}.
	 * Calls a dynamic method in the format of `"op{$operation}"` - different operations should have different `op*` methods, like
	 * {@link opExit} or {@link opGet}.
	 * Uses {@link registerShutdownFunction} to safely close the socket, capturing different exit signals.
	 *
	 * @see {@link ServerProtocol::__call} Used to handle unidentified operations.
	 * @see DropProtocol::readRequest
	 */
	private function listeningLoop() {
		$this->registerShutdownFunction();
		while(true) {
			$to_read = array_merge(
				[$this->mainSocket],
				array_map(function($client) { return $client->socket; }, $this->clients)
			);

			//blocking call. when it returns it means there's new stuff
			if (@socket_select($to_read, $to_write = [], $except = [], null) === false)
				die("Ops! Something weird happened while waiting for new stuff to come in. Maybe you closed me?\n");

			if (in_array($this->mainSocket, $to_read)) {
				$this->newClient();
				ArrayHelper::unsetByValue($to_read, $this->mainSocket);
			}

			foreach ($to_read as $read) {
				extract($this->readRequest($read));

				if (isset($head['operation']) && !empty($head['operation'])) {
					if ($this->needAuthentication($head['operation']) && !$this->getClientFromSocket($read)->logged()) {
						$this->response($read, $head['operation'], self::ST_UNAUTHORIZED);
					}
					else {
						$operation = ucfirst(strtolower($head['operation']));
						$this->{"op$operation"}($read, $head['path'], $head['headers'], $body);
					}
				}
				else {
					$this->response($read, self::OP_ERROR, self::ST_BAD_REQUEST, [], 'Operation was not identified');
				}
			}
		}
	}

	/**
	 * Registers {@link shutdown} to be run before the script ending. Captures `SIGTERM`, `SIGQUIT` and `SIGINT`.
	 */
	private function registerShutdownFunction() {
		declare (ticks = 1);
		$end = function() { $this->shutdown(); };
		register_shutdown_function($end);
		pcntl_signal(SIGTERM, $end);
		pcntl_signal(SIGQUIT, $end);
		pcntl_signal(SIGINT, $end);
	}

	/**
	 * Creates a new connection to a client using `socket_accept()`.
	 * @return ClientMachine Places the new client object in the {@link clients} array and returns it.
	 */
	private function newClient() {
		if (($new_client = @socket_accept($this->mainSocket)) === false) {
			$errno = socket_last_error($this->mainSocket);
			if ($errno)
				die("Could not accept connection: [$errno] ".socket_strerror($errno)."\n");
		}
		else {
			echo "New client connected!\n";
			return $this->clients[] = new ClientMachine($new_client, $this->debug);
		}
	}

	/**
	 * Disconnects the client and removes it from the {@link clients} pool. May optionally run a callback.
	 * @param ClientMachine $client the client to be disconnected
	 * @param callable $before_close A callback to run after removing the client from the pool and immediatelly before
	 *	calling `socket_close()`. Receives the {@link ClientMachine::socket} as argument.
	 */
	private function closeClient(ClientMachine $client, callable $before_close = null) {
		$username = $client->username;
		ArrayHelper::unsetByValue($this->clients, $client);
		if ($before_close) $before_close($client->socket);
		socket_close($client->socket);
		$this->debugMsg("The user '$username' has been logged out.");
	}

	/**
	 * Gives the client object related to the given $socket.
	 * @param resource $socket
	 * @return ClientMachine
	 */
	protected function getClientFromSocket($socket) {
		$client = ArrayHelper::clear(
			array_filter(
				$this->clients,
				function($client) use ($socket) { return $client->socket == $socket; }
			)
		);

		if (sizeof($client) == 0 || !$client[0] instanceof ClientMachine)
			throw new OutOfRangeException("Socket not found in the clients array.");
		else
			return $client[0];
	}

	/**
	 * Returns the path for a client's file.
	 * @param resource $socket
	 * @param string $file
	 * @return string
	 * @see ClientMachine::getUserDir
	 */
	protected function getPathOfClientFile($socket, $file) {
		return $this->getClientFromSocket($socket)->getUserDir($this->dir).DIRECTORY_SEPARATOR.$file;
	}

	/**
	 * Tries to close all client connections and then the main socket before `exit()`.
	 */
	public function shutdown() {
		if (is_resource($this->mainSocket)) {
			if (sizeof($this->clients)) {
				echo "\nClosing all client connections: ";
				foreach($this->clients as $i => $client) {
					echo ($i+1).'..';
					if (is_resource($client->socket)) socket_close($client->socket);
					echo '! ';
				}
			}
			else {
				echo "\nNo clients to close!";
			}
			echo "\nClosing main socket... ";
			socket_shutdown($this->mainSocket);
			socket_close($this->mainSocket);
			echo "Done! Bye ;)\n\n";
		}
	}

	/**
	 * Calls {@link shutdown()}.
	 */
	public function __destruct() { return $this->shutdown(); }
}

/**
 * Client object used by the main {@link Server} class, to store information about its clients.
 * @package Application
 * @todo Should extend one of the SPL Array classes to provide array-like data access (through [])
 *
 * @property string $summaryName The name for the choosen summary function
 * @property resource $socket Socket to be used to communicate with this client
 * @property string $username The username for this client
 * @property boolean $logged If this client is logged or not
 */
class ClientMachine {
	use CliAndFiles, SummaryFunctions;

	/** @var resource
	 * The socket to communicate with the client. */
	private $socket;
	/** @var boolean
	 * If this client is logged in and authorized to make operations. Should be set using {@link login}. */
	private $logged = false;
	/** @var string
	 * The user name. Should be set using {@link login}. */
	private $username;
	/** @var string
	 * The folder where files for this user are stored. */
	private $user_dir;

	/**
	 * Creates a new ClientMachine object.
	 * @param resource $socket the socket to communicate with the client.
	 * @param boolean $debug If the application is in debug mode.
	 */
	public function __construct($socket, $debug = false) {
		$this->socket = $socket;
		$this->debug = $debug;
	}

	/**
	 * Returns whether the client is authorized or not.
	 * @return boolean
	 */
	public function logged()	{ return $this->logged; }
	/**
	 * Unmarks the client as authorized.
	 */
	public function logout()	{ $this->logged = false; }
	/**
	 * Marks the client with the given `$username` as authorized.
	 * @param string $username
	 */
	public function login($username) {
		$this->logged = true;
		$this->username = $username;
	}

	/**
	 * Magic method implementing `set` for various internal properties.
	 * @param string $name property name
	 * @param mixed $value property value
	 * @throws UnexpectedValueException When the set value is not allowed. Further details are given in the message.
	 * @see summaryName
	 * @see socket
	 * @see logged
	 * @see username
	 * @see login
	 */
	public function __set($name, $value) {
		switch ($name) {
			case 'summaryName':
				$this->setSummary($value);
			break;

			case 'socket':
				if (!is_resource($value))
					throw new UnexpectedValueException("Invalid resource supplied for ClientMachine::socket.");
				else
					$this->socket = $resource;
			break;

			case 'logged':
			case 'username':
				throw new UnexpectedValueException("Use ClientMachine::login() or ClientMachine::logout() instead of setting values for the logged or username properties.");
			break;
		}

		$this->debugMsg("Set value for a client: '$name'='$value'");
	}

	/**
	 * Magic method implementing `get` for various internal properties.
	 * @param mixed $name property name
	 * @see socket
	 * @see logged
	 * @see username
	 */
	public function __get($name) {
		switch ($name) {
			case 'socket':
			case 'logged':
			case 'username':
				return $this->$name;
			break;
		}
	}

	/**
	 * Returns the user directory inside the server.
	 * @param string $serverDir The current server directory.
	 * @return string The directory path.
	 */
	public function getUserDir($serverDir) {
		if ($this->user_dir) return $this->user_dir;
		else return $this->user_dir = $serverDir.DIRECTORY_SEPARATOR.$this->username;
	}
}

set_time_limit(0); //script can run forever
ob_implicit_flush(); //we can see exactly what's coming in as we get it

$server = new Server([Server::SUM_MD5_HASH, Server::SUM_BYTE_COUNT]);
$server->listen();
