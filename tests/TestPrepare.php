<?
trait TestPrepare {

	public $user_folder		= 'files/client/files';
	public $server_folder	= 'files/server';
	public $user_right		= 'charlie';
	public $pass_right		= 'echo';
	public $user_wrong		= 'xxx';
	public $pass_wrong		= 'xxx';

	static public function setUpBeforeClass() {
		if (basename(getcwd()) == 'tests') chdir('..');
	}

	/**
	 * I didn't used setUp() for this case as we cannot change the setUp() behavior given the running test.
	 */
	protected function prepare($test_case) {
		$client_folder	= $this->user_folder.DIRECTORY_SEPARATOR;
		$server_folder	= $this->server_folder.DIRECTORY_SEPARATOR.$this->user_right.DIRECTORY_SEPARATOR;
		$case_folder	= 'tests'.DIRECTORY_SEPARATOR.'cases'.DIRECTORY_SEPARATOR.$test_case.DIRECTORY_SEPARATOR;

		foreach(['client', 'server'] as $program) {
			foreach(scandir(${$program.'_folder'}) as $file) {
				if (is_dir(${$program.'_folder'}.$file) && $file != '.gitignore') continue;
				unlink(${$program.'_folder'}.$file);
			}
			foreach(scandir($case_folder.$program)	as $file) {
				if (is_dir($case_folder.$program.DIRECTORY_SEPARATOR.$file)) continue;
				copy($case_folder.$program.DIRECTORY_SEPARATOR.$file, ${$program.'_folder'}.DIRECTORY_SEPARATOR.$file);
			}
		}

		/** @todo generation of this variables should be tested too, as there's some method for this! */
		copy($case_folder.'..'.DIRECTORY_SEPARATOR.$test_case.DIRECTORY_SEPARATOR.'md5-state',
			$client_folder.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'states'.DIRECTORY_SEPARATOR.'MD5_hash-'.strtr(
				rtrim($client_folder, DIRECTORY_SEPARATOR)
			, '\/', '--'));
		copy($case_folder.'..'.DIRECTORY_SEPARATOR.$test_case.DIRECTORY_SEPARATOR.'byte-state',
			$client_folder.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'states'.DIRECTORY_SEPARATOR.'byte_count-'.strtr(
				rtrim($client_folder, DIRECTORY_SEPARATOR)
			, '\/', '--'));
	}
}