<?
include_once 'TestPrepare.php';

class UseCasesTest extends PHPUnit_Framework_TestCase {

	use TestPrepare;

	private function runBasicClientTest() {
		$num = substr(debug_backtrace()[1]['function'], strlen('testCase'));
		$this->prepare($num);
		$result = shell_exec("./client.php -u $this->user_right -q $this->pass_right -d $this->user_folder");
		$this->assertEquals("Attempting to connect to localhost:19905... Connected!\nLogged out successfully. Closing socket... Closed.\n", $result);
	}

	public function testCase3() { $this->runBasicClientTest(); }
	public function testCase4() { $this->runBasicClientTest(); }
	public function testCase5() { $this->runBasicClientTest(); }
	public function testCase6() { $this->runBasicClientTest(); }
	public function testCase7() { $this->runBasicClientTest(); }
	public function testCase8() { $this->runBasicClientTest(); }
	public function testCaseBinaryGet() { $this->runBasicClientTest(); }
	public function testCaseBinaryPut() { $this->runBasicClientTest(); }

}