<?
include_once 'TestPrepare.php';

class ExceptionsTest extends PHPUnit_Framework_TestCase {

	use TestPrepare;

	public function testBadLogin() {
		$result = shell_exec("./client.php -u $this->user_wrong -q $this->pass_right -d $this->user_folder");
		$this->assertRegExp('/.*Bad login.*/', $result, 'Wrong user');

		$result = shell_exec("./client.php -u $this->user_right -q $this->pass_wrong -d $this->user_folder");
		$this->assertRegExp('/.*Bad login.*/', $result, 'Wrong password');

		$result = shell_exec("./client.php -u $this->user_wrong -q $this->pass_wrong -d $this->user_folder");
		$this->assertRegExp('/.*Bad login.*/', $result, 'Wrong user and password');
	}

}